/*********************

Inspired by the example code for the Adafruit RGB Character LCD Shield and Library
This file was written by OPOLKA Yohann at Shinshu University from January 2019
Current revision: Version 2.0 - 2019.9.3

**********************/

// include the library code:
#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>
#include <avr/eeprom.h>


// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// These #defines make it easy to set the backlight color
#define OFF 0x0
#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

// These are for the motor driver speed and direction control
#define pwm 11
#define dir 12


// *************************************
// Some structure definitions to make everything easier
typedef struct {
  char colorName[8]; // size should be at least one more than the actual number of chars
  byte color;
} colorEntry; // Entry for the color selection menu

typedef void (*buttonAction)(); // Prototype needed for associating buttons with actions

typedef void (*updateMethod)(); // Prototype needed for updating the page content for each menu

typedef struct {
  char title[17]; // Menu title
  bool canUP; // this and the following define whether the user can press buttons
  bool canDOWN;
  bool canRIGHT;
  bool canLEFT;
  buttonAction UPAction;    // this and the following allow the association of
  buttonAction DOWNAction;  // different functions with each button for each menu
  buttonAction RIGHTAction;
  buttonAction LEFTAction;
  updateMethod contentUpdate;  // Each menu has its own update function, or may share the same
} menuEntry; // Everything associated with the different menus

typedef struct{
  char title[17];
  char content[17];
} pageDisplay;  // What will finally be displayed on the LCD screen (2 lines of 16 chars)

// *************************************
// Declaration of function prototypes needed for the menus, etc.
void nf(); // A function that does nothing;
void monitorUpdate();
void monitorUP();
void monitorDOWN();
void toSpeedSetting();
void setSpeedUP();
void setSpeedDOWN();
void setSpeedRIGHT();
void setSpeedLEFT();
void setSpeedUpdate();
void setSpeedStepsUP();
void setSpeedStepsDOWN();
void setSpeedStepsRIGHT();
void setSpeedStepsLEFT();
void setSpeedStepsUpdate();
void setStepsFreqUP();
void setStepsFreqDOWN();
void setStepsFreqLEFT();
void setStepsFreqRIGHT();
void setStepsFreqUpdate();
void colorMenuRIGHT();
void colorMenuLEFT();
void colorMenuUpdate();
void saidNo();
void stopSaidYes();
void lastSaidYes();
void saveSaidYes();
void readSaidYes();
void yesNoUpdate();

// Menus declaration and default settings
// Content of the different menus and possible actions
menuEntry menu[10] = { // UP, DOWN, RIGHT, LEFT ..... Last is the contentUpdate method
  {"MONITOR         ", 1, 1, 1, 1, monitorUP, monitorDOWN, toSpeedSetting, toSpeedSetting, monitorUpdate},                              // 0
  {"MAX SPEED       ", 1, 1, 1, 1, setSpeedUP, setSpeedDOWN, setSpeedRIGHT, setSpeedLEFT, setSpeedUpdate},                              // 1
  {"SPEED STEPS     ", 1, 1, 1, 1, setSpeedStepsUP, setSpeedStepsDOWN, setSpeedStepsRIGHT, setSpeedStepsLEFT, setSpeedStepsUpdate},     // 2
  {"STEPS TIME FREQ.", 1, 1, 1, 1, setStepsFreqUP, setStepsFreqDOWN, setStepsFreqRIGHT, setStepsFreqLEFT, setStepsFreqUpdate},          // 3
  {"READ CONFIG.    ", 0, 0, 1, 1, nf, nf, saidNo, readSaidYes, yesNoUpdate},                                                           // 4
  {"SAVE CONFIG.    ", 0, 0, 1, 1, nf, nf, saidNo, saveSaidYes, yesNoUpdate},                                                           // 5
  {"SCREEN COLOR    ", 0, 0, 1, 1, nf, nf, colorMenuRIGHT, colorMenuLEFT, colorMenuUpdate},                                             // 6
  {"SET SPEED       ", 1, 1, 1, 1, setSpeedUP, setSpeedDOWN, setSpeedRIGHT, setSpeedLEFT, setSpeedUpdate},                              // 7
  {"SET LAST SPEED  ", 0, 0, 1, 1, nf, nf, saidNo, lastSaidYes, yesNoUpdate},                                                          // 8
  {"STOP            ", 0, 0, 1, 1, nf, nf, saidNo, stopSaidYes, yesNoUpdate}                                                            // 9
};
uint8_t menuIndex=0; // Sets the starting default menu page

// Content of the color selection menu
colorEntry colorMenu[8] = {
  {"WHITE  ", WHITE},   // 0
  {"TEAL   ", TEAL},    // 1
  {"BLUE   ", BLUE},    // 2
  {"VIOLET ", VIOLET},  // 3
  {"RED    ", RED},     // 4
  {"GREEN  ", GREEN},   // 5
  {"YELLOW ", YELLOW},  // 6
  {"OFF    ", OFF}      // 7
};
uint8_t colorMenuIndex=0; // Sets the starting default color

const char arrows[4] = { char(127), char(165), char(126) };
const char leftArrow[2] = { char(127) };
const char rightArrow[2] = {char(126) };
const char point[2] = {char(165) };

// Declaration and default title and content of the displayed page
pageDisplay page;

uint16_t motorSpeed=0; // Sets the starting default speed
uint16_t maxMotorSpeed=16000; // Sets the default max speed // could be read from EEPROM ??
uint16_t speedChangeStepTime=2; // Time interval between two steps in speed increase or decrease (value in 1/4 of seconds => 2 = 2*1/4 = 0.5s)
uint16_t speedChangeStep=250; // Speed step to be used for speed increase or decrease (value in rpm)
uint16_t lastSpeed=6000;

// *************************************
// Functions and methods implementations
void nf(){}; // Implementation of a function that does nothing for some menus

void caformat(char *out, char* in) {
  sprintf(out, "%s", in); // casts a char array into another one, larger. Maybe the size should better be controlled.
}
void caformat(char *out, const char* in) {
  sprintf(out, "%s", in); // casts a char array into another one, larger. Maybe the size should better be controlled.
}
void caformat(char *out, uint8_t in) {
  sprintf(out, "%i", in); // casts an integer into a char arrays, the array should be bigger than the number of digits, refer to digit2char otherwise
}
void caformat(char *out, char *in1, char *in2) {
  sprintf(out, "%s%s", in1, in2); // casts 2 char arrays into one
}
void caformat(char *out, char *in1, const char *in2) {
  sprintf(out, "%s%s", in1, in2); // casts 2 char arrays into one
}
void caformat(char *out, uint16_t in1, char *in2) {
  sprintf(out, "%i%s", in1, in2); // casts 1 int and 1 char array into a bigger char array
}
void caformat(char *out, char *in1, char *in2, char *in3){
  sprintf(out, "%s%s%s", in1, in2, in3); // casts 3 char arrays into one
}
void caformat(char *out, uint16_t in1, char *in2, char* in3) {
  sprintf(out, "%i%s%s", in1, in2, in3); // casts 1 int and 2 char arrays into a bigger char array
}
void caformat(char *out, char *in1, char *in2, char *in3, char *in4){
  sprintf(out, "%s%s%s%s", in1, in2, in3, in4); // casts 4 char arrays into one
}
void caformat(char *out, uint16_t in1, char *in2, char *in3, char *in4){
  sprintf(out, "%i%s%s%s", in1, in2, in3, in4); // casts 4 char arrays into one
}
void caformat(char *out, uint16_t in1, char *in2, char *in3, uint16_t in4){
  sprintf(out, "%i%s%s%i", in1, in2, in3, in4); // casts 4 char arrays into one
}
void caformat(char *out, char *in1, char *in2, char *in3, char *in4, char *in5){
  sprintf(out, "%s%s%s%s%s", in1, in2, in3, in4, in5); // casts s char arrays into one
}
void caformat(char *out, char *in1, char *in2, char *in3, char *in4, char *in5, char *in6){
  sprintf(out, "%s%s%s%s%s%s", in1, in2, in3, in4, in5, in6); // casts s char arrays into one
}

void gotoMenuPage(uint8_t index) {  // Function for going to a page by another mean than by pressing the Select button
  menuIndex = index;
  lcd.clear();
  lcd.setCursor(0,0);
  caformat(page.title, menu[menuIndex].title);
  lcd.print(page.title);
  //Serial.println(String(page.title)+"::"+String(page.content));
}

unsigned int time = millis()/250;   // Timekeeper for the blinking function
bool blinked = 0;   // Indicates if blinking is already being performed

void selectionBlink(int c) {   // Function for performing "blinking" of a specific character displayed at position 'c' (for 'column') on the second line
  if (((millis()/250-time) > 3) & (!blinked)) {
    page.content[c] = ' ';
    blinked=1;
    time=millis()/250;
  }
  if (((millis()/250-time) > 1) & (blinked)) {
    blinked=0;
    time=millis()/250;
  }
}

unsigned long motorTime = millis()/250;  // A timer used for checking the interval between two speed change steps
uint16_t motorSpeedAct = 0;  // "Actual" speed value used for setting the analog output
int pwm_value=0;   // For the output value to the motor
char changeState[11];  // Will contain indications about the change in speed
bool changing=0;  // boolean for indicating an ongoing change in speed, will be used for blinking
char spacing[6];  // spacing needed to keep 'changeState' always at the same position on the right
unsigned int timeReached;  // For keeping an estimation of the time left before set speed reached
unsigned int timeLeft;  // Actual estimation to be displayed
char timeLeftChar[4];

void motorSpeedChange() {  // Method for increasing and decreasing the motor speed more or less slowly, step by step
  if ((motorSpeedAct != motorSpeed) && ((unsigned long)(millis()/250-motorTime) > ((unsigned long)(speedChangeStepTime-1)))) {  // check the value of that steptime variable set earlier
    if (changing == 0) changing = 1;  // sets the 'changing' variable if it wasn't already set
    if (motorSpeedAct < motorSpeed) {  // the actual speed needs to be increased
      if ((motorSpeed - motorSpeedAct) >= (speedChangeStep)) {  // if the total gap is bigger than the increase step
        //Serial.print("Increased speed: " + String(motorSpeedAct) + " --> ");
        motorSpeedAct += (speedChangeStep);  // one step closer to the set speed
        //Serial.println(String(motorSpeedAct) + "(+" + String((speedChangeStep)) + ") / " + String(timeReached));
        if (motorSpeedAct < 10) {           // Sets the right amount of spacings before the information is printed on screen
          caformat(spacing, "     ");
        } else if (motorSpeedAct < 100) {
          caformat(spacing, "    ");
        } else if (motorSpeedAct < 1000) {
          caformat(spacing, "   ");
        } else if (motorSpeedAct < 10000) {
          caformat(spacing, "  ");
        } else caformat(spacing, " ");
        // timeReached variable to be set ... Don't forget to put it back to 0 afterwards
        if (timeReached == 0 ) {
          timeReached = (millis()/1000) + ((float)((float)motorSpeed - (float)motorSpeedAct)/(4.0*(float)speedChangeStep))*((float)speedChangeStepTime)+1;
        }
        caformat(changeState, spacing, "+ ");
      } else {  // if the difference is less than the value of one step
        //Serial.print("Increased speed: " + String(motorSpeedAct) + " --> ");
        motorSpeedAct = motorSpeed;  // Directly take the final value
        //Serial.println(String(motorSpeedAct) + "(+" + String((speedChangeStep)) + ") / " + String(timeReached));
        // Setting timeReached to 0 just in case it would not be done elsewhere, should do no harm
        timeReached = 0;
      }
    } else {  // the actual speed needs to be decreased
      if ((motorSpeedAct - motorSpeed) >= (speedChangeStep)) {  // if the total gap is bigger than the decrease step
        //Serial.print("Decreased speed: " + String(motorSpeedAct) + " --> ");
        motorSpeedAct -= (speedChangeStep);  // one step closer to the set speed
        //Serial.println(String(motorSpeedAct) + "(+" + String((speedChangeStep)) + ")");
        if (motorSpeedAct < 10) {
          caformat(spacing, "     ");
        } else if (motorSpeedAct < 100) {
          caformat(spacing, "    ");
        } else if (motorSpeedAct < 1000) {
          caformat(spacing, "   ");
        } else if (motorSpeedAct < 10000) {
          caformat(spacing, "  ");
        } else caformat(spacing, " ");
        // timeReached variable to be set ... Don't forget to put it back to 0 afterwards
        if (timeReached ==0) {
          timeReached = (millis()/1000) + ((float)((float)motorSpeedAct - (float)motorSpeed)/(4.0*(float)speedChangeStep))*((float)speedChangeStepTime)+1;
        }
        caformat(changeState, spacing, "- ");
      } else {  // if the difference is less than the value of one step
        //Serial.print("Decreased speed: " + String(motorSpeedAct) + " --> ");
        motorSpeedAct = motorSpeed;  // Directly take the final value
        //Serial.println(String(motorSpeedAct) + "(+" + String((speedChangeStep)) + ")");
        // Setting timeReached to 0 just in case it would not be done elsewhere, should do no harm
        timeReached = 0;
      }
    }
    pwm_value = map(motorSpeedAct, 0, maxMotorSpeed, 0, 255);
    // Is it okay to send analog output only after every update? Or should it be at every iteration ?
    analogWrite(pwm,pwm_value);

    // Don't forget to reinitialise the value of the motorTime time counter !
    motorTime = millis()/250;
  } else if ((motorSpeedAct == motorSpeed) && (changing)) {
    //Serial.println("Target speed reached: " + String(motorSpeedAct) + " " + String(motorSpeed));
    caformat(changeState, spacing, "   "); // Resets the changeState variable to erase indications
    if (changing == 1) changing = 0;
    if (timeReached != 0) timeReached = 0;  // Sets timeReached back to 0;
  }
  // If analogWrite is needed at every iteration, put it here...
  //analogWrite(pwm,pwm_value);
}

// *************************************
// The following are general functions to be used by different pages that should support numeral input

bool navigatedRight; // To allow some memory of Left / Right navigation
uint8_t cursorPosition, selectedDigitValue;
char rIndicator[5]; // For dynamic changes of the indicators to the extreme right of the screen

char speedInputChar[6]; // Reminder : always make arrays bigger than their content
char speedStepsInputChar[6];
char stepsFreqInputChar[6];


void initSetValueMenu() {
  cursorPosition = 0; // For the blinking selection and increase / decrease of value
  caformat(rIndicator, arrows);
  switch (menuIndex) {
    case 7: // Set motor speed
      sprintf(speedInputChar, "%05d", motorSpeed);
      break;
    case 1: // Set max motor speed
      sprintf(speedInputChar, "%05d", maxMotorSpeed);
      break;
    case 2: // Set speed steps
      sprintf(speedStepsInputChar, "%05d", speedChangeStep);
      break;
    case 3: // Set steps frequency in time
      sprintf(stepsFreqInputChar, "%05d", speedChangeStepTime);
      break;
  }
}

void setValueUP(char* valueInputChar) {
  if (String(valueInputChar[cursorPosition]).toInt() < 9) {
    valueInputChar[cursorPosition] = ((String(valueInputChar[cursorPosition]).toInt())+1)+'0';
  }
  else valueInputChar[cursorPosition] = '0';
}

void setValueDOWN(char* valueInputChar) {
  if (String(valueInputChar[cursorPosition]).toInt() > 0) {
    valueInputChar[cursorPosition] = ((String(valueInputChar[cursorPosition]).toInt())-1)+'0';
  }
  else valueInputChar[cursorPosition] = '9';
}

void setValueRIGHT(char* valueInputChar, uint16_t maxValue, uint16_t minValue) {  // Activated when we press left in the selection menu
  if ((String(valueInputChar).toInt()) < minValue) sprintf(valueInputChar, "%05d", minValue); // check that the entered value is < max value
  setValueRIGHT(valueInputChar, maxValue);
}

void setValueRIGHT(char* valueInputChar, uint16_t maxValue) {
  if ((String(valueInputChar).toInt()) > maxValue) sprintf(valueInputChar, "%05d", maxValue); // check that the entered value is < max max
  setValueRIGHT(valueInputChar);
}

void setValueRIGHT(char* valueInputChar) {
  if (cursorPosition < 5) {  // If the cursor position is < 5 and we press the right button
    cursorPosition++;        // move the cursor to the right
    navigatedRight = 1;
    if (cursorPosition >= 5) caformat(rIndicator, "OK", rightArrow);   // if the cursor is in the 5th position (by pressing right), display ok with an arrow
  }
  else {  // If we press right while we are already in position 5 (we could have come from position 4 => navigatedRight or 0 => !navigatedRight)
    if (navigatedRight) {  // position 5 is the last step before saving and returning to menu
      switch (menuIndex) {
        case 1:
          saveValue(valueInputChar, &maxMotorSpeed);
          break;
        case 2:
          saveValue(valueInputChar, &speedChangeStep);
          break;
        case 3:
          saveValue(valueInputChar, &speedChangeStepTime);
          break;
        case 7:
          saveValue(valueInputChar, &motorSpeed);
          if (motorSpeed != 0) lastSpeed = motorSpeed;
          break;
      }
      gotoMenuPage(0);
    } else {  // position 5 here is a virtual "position -1" and pressing right returns to 0.
       cursorPosition = 0;
       navigatedRight = 1;
       caformat(rIndicator, arrows);  // replaces the "OK" printed when arriving on position 5
    }
  }
}

void setValueLEFT(char* valueInputChar, uint16_t maxValue, uint16_t minValue) {  // Activated when we press left in the selection menu
  if ((String(valueInputChar).toInt()) < minValue) sprintf(valueInputChar, "%05d", minValue); // check that the entered value is < max value
  setValueLEFT(valueInputChar, maxValue);
}

void setValueLEFT(char* valueInputChar, uint16_t maxValue) {  // Activated when we press left in the selection menu
  if ((String(valueInputChar).toInt()) > maxValue) sprintf(valueInputChar, "%05d", maxValue); // check that the entered value is < max value
  setValueLEFT(valueInputChar);
}

void setValueLEFT(char* valueInputChar) {
  if (cursorPosition > 0) {  // the cursor is not yet at 0 and we press left (might include position 5 (virtual -1) => "OK" menu
    if ((cursorPosition >= 5) & (!navigatedRight)) { // We are on position 5 and we navigated left => virtual position "-1"
    switch (menuIndex) {  // We pressed left again, so we confirmed
      case 1:
        saveValue(valueInputChar, &maxMotorSpeed);
        break;
      case 2:
        saveValue(valueInputChar, &speedChangeStep);
        break;
      case 3:
        saveValue(valueInputChar, &speedChangeStepTime);
        break;
      case 7:
        saveValue(valueInputChar, &motorSpeed);
        if (motorSpeed != 0) lastSpeed = motorSpeed;
        break;
    }
      gotoMenuPage(0);    // therefore we're leaving the page
    } else {                   // we pressed left but either we didn't come from position 0 or we are at a position < 5
      cursorPosition--;        // move the cursor to the left
      navigatedRight = 0;      // we navigated left, nor right
      if (cursorPosition < 5) caformat(rIndicator, arrows);  // replaces the "OK" printed when arriving on position 5.
    }
  }
  else {  // we pressed left while already being in position 0 => we will go to position 5 ("virutal position -1")
    cursorPosition = 5;  // moving to position 5
    navigatedRight = 0;  // indicate that we come from 0
    caformat(rIndicator, leftArrow, "OK");
  }
}

void setValueUpdate(char* valueInputChar, char* unitText) {
  if (!blinked) caformat(page.content, valueInputChar, unitText, rIndicator);
  selectionBlink(cursorPosition);
}

void saveValue(char* valueInputChar, uint16_t* value) {
  *value = (String(valueInputChar).toInt());
}


// *************************************
// Implementations for the monitor menu (0)

void monitorUP() {
  gotoMenuPage(8);
}

void monitorDOWN() {
  gotoMenuPage(9);
}

void toSpeedSetting(){
  gotoMenuPage(7);
  initSetValueMenu();
}

void monitorUpdate() {
  if (changing) {
    timeLeft = timeReached - (millis()/1000);
    sprintf(timeLeftChar, "%03d", timeLeft);
    if (!blinked) caformat(page.content, motorSpeedAct, " rpm ", changeState, timeLeftChar);
    selectionBlink(11);
  } else {
    caformat(page.content, motorSpeedAct, " rpm ", changeState, "   ");
  }
}


// *************************************
// Implementations for the max speed menu (1) and set speed menu (7)

void setSpeedUP() {
  setValueUP(speedInputChar);
}

void setSpeedDOWN() {
  setValueDOWN(speedInputChar);
}

void setSpeedRIGHT() {
  if (menuIndex==7) setValueRIGHT(speedInputChar, maxMotorSpeed); // if not setting max speed, check that speed < max speed
  else setValueRIGHT(speedInputChar);
}

void setSpeedLEFT() {  // Activated when we press left in the speed selection menu
  if (menuIndex==7) setValueLEFT(speedInputChar, maxMotorSpeed); // if not setting max speed, check that speed < max speed
  else setValueLEFT(speedInputChar);
}

void setSpeedUpdate() {
  setValueUpdate(speedInputChar, " rpm    ");
}


// *************************************
// Implementations for the speedSteps (3) and StepsFreq (4) menus

uint16_t maxSpeedChangeStepTime = 43200;

void setSpeedStepsUP(){
  setValueUP(speedStepsInputChar);
}

void setSpeedStepsDOWN(){
  setValueDOWN(speedStepsInputChar);
}

void setSpeedStepsRIGHT(){
  setValueRIGHT(speedStepsInputChar, maxMotorSpeed, 1);
}

void setSpeedStepsLEFT(){
  setValueLEFT(speedStepsInputChar, maxMotorSpeed, 1);
}

void setSpeedStepsUpdate(){
  setValueUpdate(speedStepsInputChar, " rpm    ");
}

void setStepsFreqUP(){
  setValueUP(stepsFreqInputChar);
}

void setStepsFreqDOWN(){
  setValueDOWN(stepsFreqInputChar);
}

void setStepsFreqLEFT(){
  setValueLEFT(stepsFreqInputChar, maxSpeedChangeStepTime, 1);
}

void setStepsFreqRIGHT(){
  setValueRIGHT(stepsFreqInputChar, maxSpeedChangeStepTime, 1);
}

void setStepsFreqUpdate(){
  setValueUpdate(stepsFreqInputChar, " *1/4 s ");
}


// *************************************
// Implementations for the color menu (2)

void colorMenuRIGHT() {  // Right button in the color selection menu
  if (colorMenuIndex < 7) colorMenuIndex++;
  else colorMenuIndex = 0;
  lcd.setBacklight(colorMenu[colorMenuIndex].color);
}

void colorMenuLEFT() {  // left button in the color selection menu
  if (colorMenuIndex > 0) colorMenuIndex--;
  else colorMenuIndex = 7;
  lcd.setBacklight(colorMenu[colorMenuIndex].color);
}

void colorMenuUpdate(){  // Sets page.content for the Color selection menu
  caformat(page.content, colorMenu[colorMenuIndex].colorName, "      ", arrows);
}

// *************************************
// Implementations for the set speed to max menu (8), stop menu (9), save menu (6) and read menu (5)

int eepromMaxSpeedAddr = 0;
int eepromSpeedStepsAddr = 16;
int eepromStepsFreqAddr = 32;
int eepromLastSpeedAddr = 48;
int eepromColorMenuIndexAddr = 64;

void saidNo() {
  gotoMenuPage(0);
}

void lastSaidYes() {
  motorSpeed = lastSpeed;
  gotoMenuPage(0);
}

void stopSaidYes() {
  motorSpeed = 0;
  gotoMenuPage(0);
}

void readSaidYes(){
  while (!eeprom_is_ready());
  if ((uint16_t)eeprom_read_word((uint16_t*)eepromMaxSpeedAddr) != 0) maxMotorSpeed = eeprom_read_word((uint16_t*)eepromMaxSpeedAddr);
  if ((uint16_t)eeprom_read_word((uint16_t*)eepromSpeedStepsAddr) != 0) speedChangeStep = eeprom_read_word((uint16_t*)eepromSpeedStepsAddr);
  if ((uint16_t)eeprom_read_word((uint16_t*)eepromStepsFreqAddr) != 0) speedChangeStepTime = eeprom_read_word((uint16_t*)eepromStepsFreqAddr);
  if ((uint16_t)eeprom_read_word((uint16_t*)eepromLastSpeedAddr) != 0) lastSpeed = eeprom_read_word((uint16_t*)eepromLastSpeedAddr);
  colorMenuIndex = (uint8_t)eeprom_read_word((uint16_t*)eepromColorMenuIndexAddr);
  lcd.setBacklight(colorMenu[colorMenuIndex].color);
  gotoMenuPage(0);
}

void saveSaidYes(){
  while (!eeprom_is_ready());
  eeprom_update_word((uint16_t*)eepromMaxSpeedAddr, maxMotorSpeed);
  eeprom_update_word((uint16_t*)eepromSpeedStepsAddr, speedChangeStep);
  eeprom_update_word((uint16_t*)eepromStepsFreqAddr, speedChangeStepTime);
  eeprom_update_word((uint16_t*)eepromLastSpeedAddr, lastSpeed);
  eeprom_update_word((uint16_t*)eepromColorMenuIndexAddr, (uint16_t)colorMenuIndex);
  gotoMenuPage(0);
}

void yesNoUpdate() {
  //caformat(page.content, "yes: ", leftArrow, "   no: ", rightArrow);
  caformat(page.content, leftArrow, " yes  ", point, "  no ", rightArrow);
}

// *************************************
// parts of former code to be reused someday maybe

// print the number of seconds since reset:
//lcd.print(millis()/1000);


// *************************************
// Reminder: it seems no constructor can work befor setup(), only variables and structure declarations.

/***************************************
 *  Little reminder : The LCD connections to the Arduino are :
 *      5V  --> + --> LCD pin 3 (from the left)
 *      GND --> - --> LCD pin 5 (from the left)
 *      A4  ----> LCD pin 11 (2nd from the right)
 *      A5  ----> LCD pin 12 (1st from the right)
 */

void setup() {
  // Debugging output
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Print a message to the LCD. We track how long it takes since
  // this library has been optimized a bit and we're proud of it :)
  int time = millis();
  caformat(page.title, menu[menuIndex].title);
  lcd.print(page.title);
  //lcd.setCursor(0,1);
  time = millis() - time;
  Serial.print("Setup took "); Serial.print(time); Serial.println(" ms");
  readSaidYes();
  //lcd.setBacklight(colorMenu[colorMenuIndex].color);

  // For controlling the motor driver
  pinMode(pwm, OUTPUT); // pin 11
  pinMode(dir, OUTPUT); // pin 12

  // Sets the direction of the motor ?? (test both HIGH and LOW)
  digitalWrite(dir,HIGH);
}

// *************************************
// Variables needed for runtime
uint8_t i=0;
bool pressed=0; // for smoother detection of buttons pressed and released
String incomingBytes;   // for incoming serial data

// *************************************
// Main loop
void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  menu[menuIndex].contentUpdate();
  // prints the content of the current selected page:
  lcd.print(page.content);

  uint8_t buttons = lcd.readButtons();

  if (buttons) {
    if (((buttons & BUTTON_LEFT) && (!pressed)) && (menu[menuIndex].canLEFT)) {
      menu[menuIndex].LEFTAction();
    }
    if (((buttons & BUTTON_RIGHT) && (pressed==0)) && (menu[menuIndex].canRIGHT)) {
      menu[menuIndex].RIGHTAction();
    }
    if (((buttons & BUTTON_UP) && (!pressed)) && (menu[menuIndex].canUP)) {
      menu[menuIndex].UPAction();
    }
    if (((buttons & BUTTON_DOWN) && (!pressed)) && (menu[menuIndex].canDOWN)) {
      menu[menuIndex].DOWNAction();
    }
    if ((buttons & BUTTON_SELECT) && (pressed==0)) {
      lcd.clear();
      if (menuIndex < 6) menuIndex++;
      else menuIndex = 0;
      lcd.setCursor(0,0);
      switch (menuIndex) {
        case 1:
          initSetValueMenu();
          break;
        case 2:
          initSetValueMenu();
          break;
        case 3:
          initSetValueMenu();
          break;
      }
      caformat(page.title, menu[menuIndex].title);
      lcd.print(page.title);
    }
    if (pressed==0) pressed=1;
  } else if (pressed==1) pressed=0;

  /*
  // Test of the Serial communication
  // send data only when you receive data:
  if(Serial.available() > 0 ) {
    // read the incoming byte:
    incomingBytes = Serial.readStringUntil(10);

    // say what you got:
    Serial.print("I received: ");
    Serial.print(String(incomingBytes.length())+" ");
    Serial.println(incomingBytes);
    //lcd.setCursor(0,1);
    //lcd.print(String(char(incomingBytes.toInt())));
  }
  */

  motorSpeedChange();  // Increases or decreases pwm_value at a specific time interval until the value of motorSpeed is reached

  // Note that the use of EPRROM could be interesting in some conditions... But is it really needed...
  // Other note : testing the accuracy of the rotation speed should be easier at low speed, but maybe less accurate.....
}
