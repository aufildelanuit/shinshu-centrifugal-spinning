# Source Code for the Centrifugal Spinning Machine Project

In this repository, you will find some Arduino source code for controling a motor from Arduino Uno with a Cython motor driver, a LCD screen and a 5-button input from Adafruit (Current configuration). The same code can very certainly be used for different motors and motor drivers with the same kind of input. And slight modifications could cover more cases, if, for example, a different input system is used. 

This project was created as part of my research on Centrifugal Spinning, for a double degree in Textile Engineering between Shinshu University (Japan) and ENSAIT (France).

The aim of this project is to create a relatively small centrifugal spinning machine based mainly on Arduino Uno and a 3D printed "skeleton". The spinneret is handmade using two cylindrical plastic dishes, machine screws, and two motor hubs.


In this repository, *.stl and *.scad (openSCAD) files are uploaded mostly for the purpose of allowing easier maintenance and/or modifications on the currently built machine (Shinshu University, Faculty of Textile Engineering, division of Chemistry and Materials). For more information on how to build a machine from scratch, please contact me at yohann.opolka.jp@gmail.com . 

